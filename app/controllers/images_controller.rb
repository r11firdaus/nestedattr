class ImagesController < ApplicationController
	before_action :set_image, only: %i[ show edit ]
	def index
    	@images = Image.all
  	end

  	# GET /products/1 or /products/1.json
  	def show
  	end

  	# GET /products/new
  	def new
	    @image = Image.new
  	end

  	# GET /products/1/edit
  	def edit
  	end

  	private
    # Use callbacks to share common setup or constraints between actions.
    def set_product
      @image = Image.find(params[:id])
    end
end
