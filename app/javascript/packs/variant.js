$("[data-form-prepend]").click(function(e) {
  let obj = $($(this).attr("data-form-prepend"));

  obj.map((i, e) => $(e).attr("name", (a, b) => {
    if (b) {
      let newName = b.replace("addNew", Date.now)
      $(e).attr("name", newName)
    }
    // b && $(i).attr("name", `${b}`.replace("addNew", Date.now))
  }))

  obj.insertBefore(this);
  inputVal()
  // let findEl = Array.from(obj);
  // findEl.map(e => e.name && e.setAttribute("name", e.name.replace("addNew", Date.now)));
  return false;
});

// validate price cannot be under 1
$(document).ready(() => inputVal())

const inputVal = () => {
  const input = $("input[type=number]")
  let arrInput = Array.from(input)

  arrInput.map(e => e.addEventListener('input', () => {
    e.defaultValue = 1
    if (e.value < 1) e.value = 1
  }))
}