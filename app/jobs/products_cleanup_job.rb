class ProductsCleanupJob < ApplicationJob
  queue_as :default
  sidekiq_options(retry: false)

  # def perform(*args)
  def perform(id)
    count = 0

    puts "Prepare to delete last product"
    remove = Product.destroy(id)
    
    count += 1 if remove
    puts "Product deleted, total deleted = #{count}"
  end
end