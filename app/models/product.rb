class Product < ApplicationRecord
	has_many :variants, dependent: :destroy
	has_one :image, dependent: :destroy
	accepts_nested_attributes_for :image, update_only: true
	accepts_nested_attributes_for :variants, reject_if: :all_blank, allow_destroy: true
	validates :name, presence: true
	validates :price, presence: true

	self.inheritance_column = "not_sti"

	def self.search(search)
		if search
			where(["name LIKE ?","%#{search}%"])
		else
			all
		end
	end
end