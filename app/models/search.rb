class Search < ApplicationRecord
	self.inheritance_column = "not_sti"

    def search_product
        product = Product.all 

        product = product.where(['name LIKE ?', "%#{name}%"]) if name.present?
        product = product.where(['price <= ?', price]) if price.present?

        return product
    end 
end
