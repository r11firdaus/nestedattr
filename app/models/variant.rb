class Variant < ApplicationRecord
	belongs_to :product
	validates :name, presence: true, length: { minimum: 1 }
	validates :price, presence: true, length: { minimum: 1 }
end
