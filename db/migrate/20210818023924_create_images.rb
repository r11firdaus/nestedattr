class CreateImages < ActiveRecord::Migration[6.1]
  def change
    create_table :images do |t|
      t.string :url
      t.string :alt
      t.string :caption
      t.references :product, null: false, foreign_key: 

      t.timestamps
    end
  end
end
