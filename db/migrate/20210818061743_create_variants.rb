class CreateVariants < ActiveRecord::Migration[6.1]
  def change
    create_table :variants do |t|
      t.string :name
      t.float :price
      t.references :product, null: false, foreign_key:

      t.timestamps
    end
  end
end
